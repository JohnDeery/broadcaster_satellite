#!/bin/bash

if [ "$(id -u)" != "0" ]; then
        echo "Sorry, you are not root."
        exit 1
fi

#apt-get -y update && apt-get -y upgrade && apt-get -y install gcc make php5-cli php5-mysql sshfs && apt-get -y autoremove && apt-get -y autoclean

ufw allow 22

#Declare our codec array so we can pass the correct arguments to ffmpeg at compile time
declare -a Codecs

#pkg-config
#source scripts/pkg-config

#YASM
#source scripts/yasm

#libogg
#source scripts/libogg

#lame
#source scripts/lame

#libvorbis
source scripts/libvorbis

#libtheora
#source scripts/libtheora

#libvpx8
#source scripts/libvpx8

#libvpx10
#source scripts/libvpx10

#x264
#source scripts/x264

#x265
#source scripts/x265

#xvidcore
#source scripts/xvidcore

#faac
#source scripts/faac

#FFMPEG
source scripts/ffmpeg

#mkdir -p /srv/www/broadcaster_satellite/master_server
#mkdir -p /srv/www/broadcaster_satellite/logs
#mkdir -p /srv/www/broadcaster_satellite/bin
#mkdir -p /srv/www/broadcaster_satellite/global/classes
#mkdir -p /srv/www/broadcaster_satellite/buildarea

#chown -R broadcastr:broadcastr /srv/www/broadcastr_satellite

