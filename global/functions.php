<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	date_default_timezone_set("UTC");	

	// get our environment
	if (php_sapi_name() == "cli") 
	{
		define('env', 'cli');
		define('base', dirname(dirname(__FILE__)));
		define('public_html', base.'/public_html/');
	} 
	else 
	{
		define('env', 'http');
		define('base', dirname($_SERVER['DOCUMENT_ROOT']));
		define('public_html', base.'/');
	}

	spl_autoload_register('my_autoloader');

	function my_autoloader($classname) 
	{
		$filename = base.'/global/classes/'. strtolower($classname) .'.php';
		require_once($filename);
	}

	function config_site()
	{
		switch(env)
		{
			case 'cli':
				$session_array['site_config']['general_settings'] = get_settings('general_settings');
				$session_array['site_config']['mail_server_settings'] = get_settings('mail_server_settings');
				$session_array['site_config']['advanced_settings'] = get_settings('advanced_settings');
				return $session_array;
			break;
			
			case 'http':
				if(!array_key_exists('site_config', $_SESSION) || !is_array($_SESSION['site_config']))
				{
					$_SESSION['site_config']['general_settings'] = get_settings('general_settings');
					$_SESSION['site_config']['mail_server_settings'] = get_settings('mail_server_settings');
					$_SESSION['site_config']['advanced_settings'] = get_settings('advanced_settings');
					$_SESSION['site_config']['general_settings']['full_url'] = (stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://').$_SERVER['SERVER_NAME'];
				}
			break;
		}
	}

	function check_required($required = NULL)
	{
	/*	$_SESSION['prior_post'] = array();
		$required = explode(", ", $required);
				
		foreach($required as $key=>$value) { if(empty($_POST[$value])) { $new_required.=$value.", "; } }

		if(!empty($new_required))
		{		
			foreach($_POST as $key=>$value) { $_SESSION['prior_post'][$key] = $value;	}
			$_SESSION['sub_message'] = explode(", ", $new_required);
			return 1;
		}*/
	}

	function convert_from_utc($date = NULL)
	{
		date_default_timezone_set('UTC');

		if(strlen($date) == 0)
		{
			$date = new DateTime();
		}
		else
		{
			$date = new DateTime($date);
		}

		if(!array_key_exists('time_zone', $_SESSION['user_info']['preferences']))
			$_SESSION['user_info']['preferences']['timezone']['value'] = date_default_timezone_get();

		$date->setTimezone(new DateTimeZone($_SESSION['user_info']['preferences']['timezone']['value']));
		return $date;
	}
	
	function convertPHPSizeToBytes($sSize)  
	{  
		if ( is_numeric( $sSize) ) {
		   return $sSize;
		}
		$sSuffix = substr($sSize, -1);  
		$iValue = substr($sSize, 0, -1);  
		switch(strtoupper($sSuffix)){  
		case 'P':  
			$iValue *= 1024;  
		case 'T':  
			$iValue *= 1024;  
		case 'G':  
			$iValue *= 1024;  
		case 'M':  
			$iValue *= 1024;  
		case 'K':  
			$iValue *= 1024;  
			break;  
		}  
		return $iValue;  
	}  

	function formatBytes($bytes, $precision = 2) 
	{
		$units = array('B', 'KB', 'MB', 'GB', 'TB');
	  
		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);
	  
		$bytes /= pow(1024, $pow);
	  
		return round($bytes, $precision) . ' ' . $units[$pow];
	}	

	function generate_jQuery($plugins=NULL)
	{
		$settings = get_settings("advanced_settings");
		$jquery_version = $settings['jquery_version']['value'];
		$jquery_ui_version = $settings['jquery_ui_version']['value'];
		
		echo '		
				<script type="text/javascript">
					window.jQuery || document.write(unescape("%3Cscript src=\"http://ajax.googleapis.com/ajax/libs/jquery/'.$jquery_version.'/jquery.min.js\"%3E%3C/script%3E"));
				</script>	
				<script type="text/javascript">
					window.jQuery || document.write(unescape("%3Cscript src=\"/includes/javascript/jquery/'.$jquery_version.'_min.js\"%3E%3C/script%3E"));
				</script>	
			';
		if(isset($plugins))
		{
			$plugins=explode(',', $plugins);
			foreach($plugins as $plugin_request)
			{
				switch($plugin_request)
				{
					case 'ui':
					echo '
						<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/'.$jquery_ui_version.'/jquery-ui.min.js"></script>				
						<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/'.$jquery_ui_version.'/themes/overcast/jquery-ui.css" type="text/css" rel="stylesheet">
						<script type="text/javascript">
							if (typeof jQuery == "undefined")
							{
								document.write(unescape("%3Cscript src=\"/includes/javascript/jquery/plugins/ui/'.$jquery_ui_version.'/jquery-ui.min.js\" type=\"text/javascript\"%3E%3C/script%3E"));
								document.write(unescape("%3Clink href=\"/includes/javascript/jquery/plugins/ui/'.$jquery_ui_version.'/jquery-ui.min.css\" type=\"text/css\" rel=\"stylesheet\"%3E%3C/script%3E"));
							}
						</script>						
					 ';
					break;
			
					default:
					if(file_exists(base.'/includes/javascript/jquery/plugins/'.$plugin_request.'.js'))
					{	
						if(is_dir(base.'/includes/javascript/jquery/plugins/'.$plugin_request))
						{
							// Load files from directory
							echo '<script type="text/javascript" src="/includes/javascript/jquery/plugins/'.$plugin_request.'/'.$plugin_request.'.js"></script>';
							if(web_root.'/includes/javascript/jquery/plugins/'.$plugin_request.'/styles')
							{
								echo '<link href="/includes/javascript/jquery/plugins/'.$plugin_request.'/styles/'.$plugin_request.'.css" type="text/css" rel="stylesheet">';
							}
						} else {
							// Load single file
							echo '
									<script type="text/javascript" src="/includes/javascript/jquery/plugins/'.$plugin_request.'.js"></script>
								';
						}
					}
					break;
				}
			}
		}
	}

	function generate_password($options=array())
	{
		if(!function_exists('password_hash'))
			require_once(base.'../global/password.php');

		if(!key_exists('password', $options))
		{
			// Create a random password
			$salt_array = "abcdefghijklmnopqrstuvwxyz0123456789";
			for ($i=0;$i < rand(10, 20);$i++) {
				$tmp = substr($salt_array, rand() % 33, 1);
				if((rand() % 33) < 10 ) { $tmp = strtoupper($tmp); }
				$pass = $pass . $tmp;
			}
		}
		else
		{
			$pass = $options['password'];
		}
		
		$clearpass = $pass;
		
		$hash = password_hash($pass, PASSWORD_BCRYPT, array("cost"=>12));
		
		$return['password'] = $hash;
		if($options['return_clear'] === TRUE) $return['clear_password'] = $clearpass;

		return $return;
	}	

	function generate_unique_id($pass = NULL)
	{
		$salt = "abcdefghijklmnopqrstuvwxyz0123456789";
		for ($i=0;$i < rand(5, 10);$i++) {
			$tmp = substr($salt, rand() % 33, 1);
			if((rand() % 33) < 10 ) { $tmp = strtoupper($tmp); }
			$pass = $pass . $tmp;
		}
		return $pass;	
	}
	
	function get_settings($column = NULL)
	{		
		$option_where = NULL;
		$parameters = NULL;
		$options = NULL;
		
		if(strlen($column) > 0)
		{
			$option_where = "WHERE option_name = :column";
			$parameters = array(':column'=>$column);
		}
		
		$sql = "
			SELECT 
				option_value 
			FROM 
				settings 
				$option_where
		";
	
		if(!isset($db))
			$db = new Database();
		
		if(count($parameters) > 0)
		{
			$options = array('parameters'=>$parameters);
		}
		
		$db->query($sql, $options);

		$row = $db->result[0];

		if(json_decode($row['option_value']))
		{
			return json_decode($row['option_value'], TRUE);
		} else {
			return $row['option_value'];
		}
	}

	function get_user_preferences()
	{
		/*
		if(!isset($db_user_prefs))
			$db_user_prefs = new Database();
	
		$sql = "
			SELECT 
				u.preferences AS 'user_prefs',
				d.option_value AS 'defaults'
			FROM 
				users u
				LEFT JOIN settings d
			WHERE
				u.ID = :user_id
				AND d.option_name = 'user_preferences_defaults'
		";
		
		print_r($sql);
		
		$options = array('parameters'=>array(":user_id"=>$_SESSION['user_info']['id']));
		
		$db_user_prefs->query($sql, $options);
		
		print_r($db_user_prefs);
		die();

		if(json_decode($db_user_prefs->result[0]['user_prefs']))
			$user_prefs = json_decode($db_user_prefs->result[0]['user_prefs'], TRUE);
		
		if(json_decode($db_user_prefs->result[0]['defaults']))
			$defaults = json_decode($db_user_prefs->result[0]['defaults'], TRUE);
			
		// We don't want to pull everything into the sesson, just the value and the type for the preference. This way, if we ever have to change the 
		// text that accompanies, we can do so. If the type changes, we want to make sure we override the user pref with the new type and value so 
		// we don't get any errors
		
		$ignored_keys = array("description", "required", "placeholder");

		// For each of the preferences found in the defaults, find out if the user already has one set. If not, we add it to the master array with
		// the default value. We'll also add that to another array so we can alert the user that a new preference was found 	
		/*
		foreach($defaults as $key=>$value)
		{
			if(array_key_exists($key, $user_prefs) && $user_prefs[$key] != $defaults[$key])
			{
				$preferences[$key] = $user_prefs[$key];
				foreach($ignored_keys as $ignored_key)
					unset($preferences[$key][$ignored_key]);
			
				$changed++;
			} elseif(!in_array($key, $user_prefs)) {
				$preferences[$key] = $defaults[$key];
				$new_options[] = $key;

				foreach($ignored_keys as $ignored_key)
					unset($preferences[$key][$ignored_key]);
			}
		}
		*/
		/*
		$_SESSION['user_info']['preferences'] = $preferences;
		
		if($changed > 0)
		{
			$preferences = json_encode($preferences);
			//save_user_prefs();
			$_SESSION['message'] = 'new_preference_found';
		}
		*/
	}
	
	function get_utc_datetime()
	{
		date_default_timezone_set('UTC');
		$date = new DateTime();
		return $date;
	}

	function getMaximumFileUploadSize()  
	{  
		return min(convertPHPSizeToBytes(ini_get('post_max_size')), convertPHPSizeToBytes(ini_get('upload_max_filesize')));  
	} 

	function mailMessage($params=NULL)
	{	
		if(!$params['to'] || !$params['subject'] || !$params['body']) die("A required field is missing. Please go back and try again.");

		if(!array_key_exists('from_address', $params) || (array_key_exists('from_address', $params) && $params['from_address'] == ''))
		{ 
			$params['from_address'] = (env == 'cli') ? $params['session_array']['site_config']['general_settings']['administrator_email']['value'] : $_SESSION['site_config']['general_settings']['administrator_email']['value'];
		}

		if(!array_key_exists('from', $params) || (array_key_exists('from', $params) && $params['from'] == '')) { $params['from'] = 'administrator'; }
	
		if(!array_key_exists('priority', $params) || (array_key_exists('priority', $params) && intval($params['priority']) == 0)) { $params['priority'] = '3'; }

		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->Host = (env == 'cli') ? $params['session_array']['site_config']['mail_server_settings']['smtp_server_address']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_server_address']['value'];	// SMTP server
		$mail->SMTPAuth = true; // turn on SMTP authentication
		$mail->SMTPSecure = "tls";   
		$mail->Port = (env == 'cli') ? $params['session_array']['site_config']['mail_server_settings']['smtp_port']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_port']['value'];
		$mail->Username = (env == 'cli') ? $params['session_array']['site_config']['mail_server_settings']['smtp_user_name']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_user_name']['value'];	// SMTP username
		$mail->Password = (env == 'cli') ? $params['session_array']['site_config']['mail_server_settings']['smtp_password']['value'] : $_SESSION['site_config']['mail_server_settings']['smtp_password']['value']; // SMTP password
		$mail->SetFrom($params['from_address'], $params['from']);

		$mail->Priority = intval($params['priority']);
		if(isset($params['bcc'])) { $mail->AddBCC($params['bcc']); }
		
		$mail_to = explode(',', $params['to']);
		foreach($mail_to as $address) { $mail->AddAddress(trim($address)); }
		
		$mail->Subject = "[".(env == 'cli' ? $params['session_array']['site_config']['general_settings']['app_name']['value'] : $_SESSION['site_config']['general_settings']['app_name']['value'])."] - ".$params['subject'];
		$mail->Body = $params['body'];
		$mail->MsgHTML($params['body']);
		
		if(isset($params['alt_body'])) { $mail->AltBody = $params['alt_body']; }
		
		$mail->isHTML(true);
		$mail->SetLanguage("en");
		
		if(!$mail->Send()) 
		{ 
			switch(env)
			{
				case 'cli':
					$logger = new Logging();
					$logger->write_to_log('errors.log', $mail->ErrorInfo);
					echo $mail->ErrorInfo;
				break;
				
				default:
					if($mail->ErrorInfo != "SMTP Error: Data not accepted.") 
					{
						$_SESSION['error'] = "mailer_too_busy";
						display_message();
						exit(); 
					}
				break;
			}
		} 
		else 
		{
			$track_email = (env == 'cli') ? $params['session_array']['site_config']['advanced_settings']['track_email']['value'] : $_SESSION['site_config']['advanced_settings']['track_email']['value'];
			if($track_email == 1) 
			{
				if(!isset($db_mail_conn))
					$db_mail_conn = new Database();

				$sql = "
					INSERT INTO email
						(`from`, `to`, `subject`, `date_registered`) 
					VALUES 
						('".$params['from_address']."', '".$params['to']."', '".$mail->Subject."', NOW())";
				//die($sql);
				$db_mail_conn->query($sql); 
			}
		}
	
	    $mail->ClearAddresses();
		return;
	}

	function print_p($message) 
	{ 
		if(is_array($message))
		{
			echo '<pre>';
			print_r($message);
			echo '</pre>';
		}
		else
		{
			echo $message;
		}
	}

	function redirect($url) { echo (empty($url) ? "Sorry, you need to supply a url" : '<script type="text/javascript">window.location="'.$url.'"</script>'); }
?>