<?php
class Video
{
	function __construct()
	{
		require_once(dirname(dirname(__FILE__)).'/functions.php');
		$this->db = new Database();
		$this->logger = new Logging();
	}

	public function getVideoDetails($id, $config = NULL)
	{
		$db = new Database();
		$logger = new Logging();
		$sql = "
			SELECT 
				v.status
				, v.title
				, v.description
				, v.uid
				, v.attributes
			FROM 
				videos v 
			WHERE
				v.uid = :uid
		";
		$options['parameters'] = array(':uid'=>$id);
		
		$db->query($sql, $options);
		
		if($db->num_rows() == 0)
		{
		
		}
		
		$row = $db->result[0];
		$attributes = json_decode($row['attributes'], TRUE);
		
		if(count($attributes) == 0) $attributes = array();

		// set all the defaults
		$video_details['player'] = array('width'=>950, 'height'=>650, 'preload'=>'auto', 'loop'=>FALSE, 'controls'=>TRUE, 'autoplay'=>FALSE);

		if(array_key_exists('player', $attributes))
		{
			// Ok, check each of the main settings and see if we have anything, otherwise, the defaults. There is probably a cleaner way to do this
			if(array_key_exists('width', $attributes['player']))
				$video_details['player']['width'] = $attributes['player'];
				
			if(array_key_exists('height', $attributes['player']))
				$video_details['player']['height'] = $attributes['player']['height'];
				
			if(array_key_exists('preload', $attributes['player']))
				$video_details['player']['preload'] = $attributes['player']['preload'];
				
			if(array_key_exists('loop', $attributes['player']))
				$video_details['player']['loop'] = $attributes['player']['loop'];

			if(array_key_exists('controls', $attributes['player']))
				$video_details['player']['controls'] = $attributes['player']['controls'];

			if(array_key_exists('autoplay', $attributes['player']))
				$video_details['player']['autoplay'] = $attributes['player']['autoplay'];
		}
		
		if(is_array($config) && count($config) > 0)
		{
			
		}

		$video_details['status'] = $row['status'];
		
		// Build out the actual movie files for the HTML5 tags
		$sql = "
			SELECT
				eq.extension
				, eq.media_type
			FROM
				encoding_queue eq
			WHERE
				eq.uid = :uid
				AND eq.status = :status
		";
		$options['parameters'] = array(':uid'=>$row['uid'], ':status'=>'finished');
		$db->query($sql, $options);

		if($db->num_rows() == 0)
		{
			$logger->write_to_log('error.log', 'Something went wrong, there were no finished videos but the status is ready!');
			return;
		}

		foreach($db->result as $video_row)
		{
			if(!file_exists(public_html.'/videos/'.$row['uid'].'/'.$row['uid'].'.'.$video_row['extension']))
			{
				$logger->write_to_log('error.log', 'We should have a video with extension '.$video_row['extension'].' but it was not found');
			}
			else
			{
				$video_details['video_files'][$video_row['media_type']] = '/videos/'.$row['uid'].'/'.$row['uid'].'.'.$video_row['extension'];
			}
		}

		// check and see if we have a poster image to show
		if(file_exists(public_html.'videos/'.$row['uid'].'/thumbnail.jpg'))
			$video_details['poster'] = '/videos/'.$row['uid'].'/thumbnail.jpg';
			
		return $video_details;
	}
	
	public function buildVideoPlayer($id, $config)
	{
		$db = new Database();
		$logger = new Logging();

		$video_details = $this->getVideoDetails($id, $config);
			
		switch($video_details['status'])
		{
			case 'ready':
				$videos = array();

				// Set up the data-setup array with anything we've defined in the attributes of this video, minus the above poster
				$data_setup = json_encode($video_details['player']);
					
				$return = '
					<link href="/includes/javascript/video-js/video-js.css" rel="stylesheet" type="text/css">
					<div id="video_area" class="text-center">
						<video id="'.$row['uid'].'" class="video-js vjs-default-skin vjs-big-play-centered center" data-setup=\''.$data_setup.'\'>
				';
					
					if(!is_array($videos) || count($videos) == 0)
						return;
					
					foreach($videos as $key=>$value)
					{
						$return.= '
								<source src="'.$value.'" type="'.$key.'" />
						';
					}
					$return.='
							</video>
						</div>

						<script src="/includes/javascript/video-js/video.js"></script>
						<script src="/includes/javascript/video-js/plugins/videojs.disableProgress.js"></script>
						<script src="/includes/javascript/video_script.js"></script>
				';
				return $return;
			break;
				
			default:
				$return = '
					<div id="video_area" class="text-center">
						<div id="video_not_found" class="center">Sorry, but there doesn\'t seem to be a video with that ID</div>
					</div>
				';
	?>
				<h1><?php echo stripslashes($row['title']); ?></h1>
				<div id="video_not_found">This video is currently encoding and will be available soon!</div>
	<?php
			break;
		}

	}


	public function generateThumbnail($video_array)
	{
		$cmd = "/usr/local/bin/ffmpeg -ss 00:00:03 -y -i '".$video_array['top_dir']."/original_videos/".$video_array['file']."' -vframes 1 -q:v 2 '".$video_array['full_video_path']."thumbnail.jpg'";
		exec($cmd, $output, $encoding_result);
		$this->setPermissions($video_array['full_video_path']."thumbnail.jpg");
		return TRUE;
	}
	
	public function generateUniqueId()
	{
		$id = NULL;

		$characters = "abcdefghijklmnopqrstuvwxyz0123456789-_";

		for ($i=0;$i < rand(10, 20);$i++)
		{
			$tmp = substr($characters, rand() % 33, 1);
			if((rand() % 33) < 10 ) { $tmp = strtoupper($tmp); }
			$id = $id . $tmp;
		}

		return $id;
	}

	public function getCurrentlyEncoding()
	{
		$sql = "
			SELECT
				eq.id
			FROM
				encoding_queue eq
			WHERE 
				eq.status = :status
		";
		$options['parameters'] = array(':status'=>'encoding');
		$this->db->query($sql, $options);
		return $this->db->num_rows();
	}
	
	public function getEncodingProgress($args=array())
	{
		$file = '/srv/www/broadcastr.professorials.com/logs/encoding_logs/'.$args['id'].'.progress.log';

		$lines = $this->tailCustom($file, $lines = 10, $adaptive = true);
		$lines = explode("\n", $lines);
		$progress = explode("=", $lines[0]);
		$progress = round((($progress[1] / $args['total_frames']) * 100), 0);
		
		$status = explode("=", $lines[9]);

		return array('progress'=>$progress, 'status'=>$status[1]);
	}
	
	public function getNextVideoForConversion()
	{
		$sql = "
			SELECT
				v.id
				, v.uid
				, v.original_name
				, v.original_ext
				, u.email_address
				, eq.cmd
				, eq.id AS 'queue_id'
				, eq.extension
			FROM
				videos v
				INNER JOIN encoding_queue eq ON eq.uid = v.uid
				INNER JOIN users u ON u.id = v.owner
			WHERE
				v.status = :v_status
				AND eq.status = :eq_status
			LIMIT 1
		";
		$options['parameters'] = array(':v_status'=>'pending', ':eq_status'=>'pending');
		$this->db->query($sql, $options);

		if($this->db->num_rows() > 0)
		{
			return $this->db->result[0];
		}
		return false;
	}

	public function setPermissions($path)
	{
		return chmod($path, 0775);
	}

	function tailCustom($filepath, $lines = 1, $adaptive = true)
	{
		// Open file
		$f = @fopen($filepath, "rb");
		if ($f === false) return false;
		// Sets buffer size
		if (!$adaptive)
		{
			$buffer = 4096;
		}
		else
		{
			$buffer = ($lines < 2 ? 64 : ($lines < 10 ? 512 : 4096));
		}
		// Jump to last character
		fseek($f, -1, SEEK_END);
		// Read it and adjust line number if necessary
		// (Otherwise the result would be wrong if file doesn't end with a blank line)
		if (fread($f, 1) != "\n") $lines -= 1;
		// Start reading
		$output = '';
		$chunk = '';
		// While we would like more
		while (ftell($f) > 0 && $lines >= 0) {
			// Figure out how far back we should jump
			$seek = min(ftell($f), $buffer);
			// Do the jump (backwards, relative to where we are)
			fseek($f, -$seek, SEEK_CUR);
			// Read a chunk and prepend it to our output
			$output = ($chunk = fread($f, $seek)) . $output;
			// Jump back to where we started reading
			fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);
			// Decrease our line counter
			$lines -= substr_count($chunk, "\n");
		}
		// While we have too many lines
		// (Because of buffer size we might have read too many)
		while ($lines++ < 0) {
			// Find first newline and remove all text before that
			$output = substr($output, strpos($output, "\n") + 1);
		}
		// Close file and return
		fclose($f);
		return trim($output);
	}

}