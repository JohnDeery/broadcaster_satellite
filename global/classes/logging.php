<?php
	class Logging
	{
		function __construct()
		{
			if (php_sapi_name() == "cli") {
				$this->base = dirname(dirname(dirname(__FILE__)));
			} else {
				$this->base = dirname($_SERVER['DOCUMENT_ROOT']);
			}
		}
		
		function write_to_log($log_file = NULL, $message = NULL, $email_options = NULL)
		{
			if(empty($log_file) || empty($message))
			{
				error_log('No log file or message passed to logging class', 0);
				return;
			}
			
			date_default_timezone_set('UTC');
			
			if(is_array($message))
				$message = print_r($message, TRUE);

			$log_file = $this->base.'/logs/'.$log_file;			
			$message = "[".date('Y-m-d H:i:s')."] - $message\n";
			error_log($message, 3, $log_file);

			if(is_array($email_options) && sizeof($email_options) > 0)
			{
				// also send as email to the place defined in the options

				$ia = array (
					'to' => (array_key_exists('to', $email_options)) ? $email_options['to'] : '',
					'from' => 'admin',
					'from_address' => 'john.deery@gmail.com',
					'priority' => (array_key_exists('priority', $email_options)) ? $email_options['priority'] : 7,
					'subject' => (array_key_exists('subject', $email_options)) ? $email_options['subject'] : 'Log Message',
					'body' => $message
				);

				mail_message($ia);
			}			
		}
/*
		function write_to_db($log_array = NULL)
		{
			if($log_array == NULL || !is_array($log_array))
				return;
			
			$sql = "
				SELECT 
					id
				FROM
					logs_type
				WHERE
					name = :log_name
			";
			$options['parameters'] = array(":log_name"=>$log_array['log_name']);
		
			$this->db->query($sql, $options);
			
			if($this->db->num_rows() == 0)
			{
				//We didn't find this log type? Let's insert it and then get the ID and use it
				$sql = "
					INSERT INTO
						logs_type
					SET
						`name` = :name
				";
			
				$options['parameters'] = array(":name"=>$log_array['log_name']);
				$this->db->query($sql, $options);
				
				$row['ID'] = $this->db->last_insert_id();			
			} else {
				$row = $this->db->result[0];
			}
		
			$sql = "
				INSERT INTO 
					logs
				SET
					log_type = :log_type
					, message = :message
					, user_id = :user_id
			";	
	
			$options['parameters'] = array(":log_type"=>$row['id'], ":message"=>$log_array['message'], ":user_id"=>$log_array['user_id']);
			$this->db->query($sql, $options);
		}
*/
	}
?>