<?php
class Database
{
	var $Link_ID  = 0;																// Result of connection.
	var $result   = array();														// current result.
	
	var $Errno    = 0;																// error state of query...
	var $Error    = "";
	
	function __construct()
	{
		if (php_sapi_name() == "cli") {
			$this->base = dirname(dirname(dirname(__FILE__)));
		} else {
			$this->base = dirname($_SERVER['DOCUMENT_ROOT']);
		}
	
		// Parse the ini file to get db info
		$db_info = parse_ini_file($this->base.'/global/config.ini.php', true);

		$this->Host		= $db_info['app_db_info']['host'];     		// Hostname of our MySQL server. Set in config.ini.php
		$this->Database	= $db_info['app_db_info']['db_name'];        // Logical database name on that server. Set in config.ini.php
		$this->User		= $db_info['app_db_info']['un'];             // User and Password for login. Set in config.ini.php
		$this->Password	= $db_info['app_db_info']['pw'];
		if(array_key_exists('no_db', $db_info['app_db_info']))
			$this->No_DB	= $db_info['app_db_info']['no_db'];		// This should only be used during setup, as we need to create the DB

		$this->logger = new Logging();
	}

	//-------------------------------------------
	//    Connects to the database
	//-------------------------------------------
	public function connect()
	{
		if(!$this->Link_ID)
		{
			try 
			{
				$dbname = (property_exists($this, 'No_DB')) ? '' : "dbname=".$this->Database;
				$db_string = "mysql:host=".$this->Host.";$dbname";
				$dbh = new PDO($db_string,$this->User,$this->Password);
			}  
			catch(PDOException $e) {  
				$this->halt($e->getMessage());  
			}
			$this->Link_ID = $dbh;
			//$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		}
		
		if( !$this->Link_ID )
			$this->halt( "Link-ID == false, connect failed" );
	} 
	// end function connect

	//-------------------------------------------
	//    Queries the database
	//-------------------------------------------
	public function query($sql, $options=NULL)
	{
		$this->connect();
		$this->result = NULL;

		$query = $this->Link_ID->prepare($sql);
		
		if(!$query)
		{
			echo "SQL Error:\n";
			$this->Link_ID->errorInfo();
			$this->logger->write_to_log('sql.error.log', $this->Link_ID->errorInfo());
			die();
		}
		
		if(isset($options) && count($options) > 0 && array_key_exists('parameters', $options) && is_array($options['parameters']))
		{
			$bound_parameters = $options['parameters'];
		}
		else
		{
			$bound_parameters = array();
		}
		
		try {
			$query->execute($bound_parameters);
		}
		catch(Exception $e)
		{
			echo "SQL Error";
			var_dump($e->getMessage());
			$this->logger->write_to_log('sql.error.log', $e->message());
		}
		
		$this->result = $query->FetchAll(PDO::FETCH_ASSOC);
	} // end function query

	//-------------------------------------------
	//    If error, halts the program
	//-------------------------------------------
	function halt( $msg )
	{
		printf("<strong>Database error:</strong> %s<br>", $msg );
		printf("<strong>MySQL Error</strong>: %s (%s)<br>", $this->Errno, $this->Error);
		$this->logger->write_to_log('sql.error.log', 'MySQL Error: '.$msg);
		$this->logger->write_to_log('sql.error.log', $this->Errno);
		$this->logger->write_to_log('sql.error.log', $this->Error);

		die();
	} // end function halt

	//-------------------------------------------
	//    Returns the number of rows  in a recordset
	//-------------------------------------------
	public function num_rows()
	{
		if(is_array($this->result))
		{
			return count($this->result);
		} else {
			return 0;
		}
	} 
	// end function numRows

	//-------------------------------------------
	//    Returns the Last Insert Id
	//-------------------------------------------
	function last_insert_id()
	{
		return $this->Link_ID->lastInsertId();
	}
	
	function debug()
	{
		return $this->result->debugDeumpParams();
	}
}
?>